<?php require('../blog/wp-load.php'); ?>
<!DOCTYPE html>
<html lang="ja"><!-- InstanceBegin template="/Templates/base.dwt" codeOutsideHTMLIsLocked="false" -->
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
<meta charset="UTF-8">
<!-- InstanceBeginEditable name="title" -->
	<title>エントリーフォーム 正社員・契約社員 ｜ 採用情報 ｜ スペース・アズ／奈良県生駒市・大阪府堺市の有料老人ホーム・デイサービス</title>
	<!-- InstanceEndEditable -->
<meta name="viewport" content="width=device-width">
<meta name="format-detection" content="telephone=no">
<meta name="keywords" content="有料老人ホーム,ホーム,サ高住,訪問介護,大阪府,奈良県,プラスワンリビング,コージーガーデン,スペース・アズ。" />
<meta name="description" content="有料老人ホーム・サ高住・訪問介護のスペース・アズ【奈良県・大阪府】。" />
<!-- InstanceBeginEditable name="ogp" -->
	<meta property="og:title" content="エントリーフォーム 正社員・契約社員 ｜ 採用情報 ｜ スペース・アズ／奈良県生駒市・大阪府堺市の有料老人ホーム・デイサービス" />
	<meta property="og:type" content="article" />
	<meta property="og:description" content="有料老人ホーム・サ高住・訪問介護のスペース・アズ【奈良県・大阪府】。" />
	<meta property="og:url" content="http://s-az.jp/recruit/entry_full.html" />
	<meta property="og:image" content="" />
	<meta property="og:locale" content="ja_JP" />
	<meta property="og:site_name" content="スペース・アズ／奈良県生駒市・大阪府堺市の有料老人ホーム・デイサービス" />
	<!-- InstanceEndEditable -->

<!-- Google Fonts -->
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@300;400;500;700&family=Noto+Serif+JP&display=swap" rel="stylesheet">
<!-- END Google Fonts -->
<link rel="stylesheet" href="../parts/css/reset.css">
<link rel="stylesheet" href="../parts/css/common.css">
<!-- InstanceBeginEditable name="css" -->
	<link rel="stylesheet" href="../parts/css/validationEngine.jquery.css">
	<link rel="stylesheet" href="../parts/css/info.css">
	<link rel="stylesheet" href="../parts/css/recruit.css">
	<!-- InstanceEndEditable -->
<script src="../parts/js/jquery-3.2.1.min.js"></script> 
<script src="../parts/js/script.js"></script>
<!-- InstanceBeginEditable name="js" -->
	<script src="../parts/js/jquery.validationEngine.js"></script>
	<script src="../parts/js/jquery.validationEngine-ja.js"></script>
	<script>
		$(function() {
			$("#form").validationEngine({
				promptPosition: "topLeft"
			});
		});
		$(function() {
			var id = location.search.match(/[&?]id=(.+?)(?:&|$)/);
			if (id) {
				$('#' + id[1]).addClass('is-active');
			}
		});
	</script>
	<!-- InstanceEndEditable -->
<!-- InstanceParam name="en" type="text" value="recruit" -->
    
    
    
    
    <!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-3PTQBYDKR6"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-3PTQBYDKR6');
</script>
    
    
    
    
</head>

<body class="recruit">
<header class="header">
	<div class="inner_wide">
		<h1 class="header_logo"><a href="../"><img src="../parts/images/common/header_logo.png" alt="ロゴマーク"></a></h1>
		<ul class="header_info pc">
			<li class="tel"><a href="tel:0120834177"><img src="../parts/images/common/header_tel.png" alt="フリーダイヤル 0120-834-177"></a></li>
			<li class="contact"><a href="../info/">見学・体験入居・お問い合わせ</a></li>
		</ul>
		<nav class="gnav">
			<button type="button" class="gnav_button"></button>
			<div class="gnav_menu_wrapper">
				<ul class="gnav_menu">
					<li class="sp"> <a href="../">TOP</a> </li>
					<li> <a href="../guidance/">施設のご紹介</a> </li>
					<li> <a href="../medical/">医療・介護体制</a> </li>
					<li> <a href="../flow/">ご入居までの流れ</a> </li>
					<li> <a href="../faq/">よくあるご質問</a> </li>
					<li> <a href="../about/">私たちについて</a> </li>
					<li> <a href="../recruit/">採用情報</a> </li>
				</ul>
				<ul class="header_info sp">
					<li class="tel">
						<h6 class="ttl">見学・入居のご相談・お問い合わせはお気軽に</h6>
						<a href="tel:0120834177"><img src="../parts/images/common/header_tel.png" alt="フリーダイヤル 0120-834-177"></a>
						<div class="time">お電話受付時間／9：00～17：00（土日祝休）</div>
					</li>
					<li class="contact"><a href="../info/">見学・体験入居・お問い合わせ</a></li>
				</ul>
			</div>
		</nav>
	</div>
</header>
<main class="main"><!-- InstanceBeginEditable name="main" -->
		<section class="mv">
			<div class="inner">
				<h1 class="mv_ttl">採用情報：エントリー</h1>
			</div>
		</section>
		<section id="" class="entry entry_full contact_mail recruiting">
			<!--<section class="recruiting anchor" id="recruiting">-->
			<div class="content">
				<div class="inner">
					<div class="entry_btn_back"><a href="../recruit/">採用情報へ戻る</a></div>
					<h2 class="lttl_line entry_lttl"><span>エントリーフォーム：<br class="sp">正社員・契約社員</span></h2>
					<h3 class="mttl"><span>現在募集中の職種</span></h3>
					<?php
					$wp_query = new WP_Query();
					$args = array(
						'posts_per_page' => -1, //表示する記事の数
						'post_type' => 'recruit', //投稿タイプ名
						'tax_query'      => array(
							array(
								'taxonomy' => 'recruit_status',
								'field' => 'slug',
								'terms' => 'part-time',
								'operator' => 'NOT IN',
							),
						)
					);
					$wp_query->query($args);
					?>
					<?php if ($wp_query->have_posts()) : ?>
						<ul class="entry_tab mgt0">
							<?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
								<li><a href="#<?php echo get_the_ID(); ?>"> <?php the_title(); ?></a></li>
							<?php endwhile; ?>
						</ul>
					<?php else : //記事が無い場合 
					?>
						<p>ただいまの募集はございません。</p>
					<?php endif; ?>

					<?php wp_reset_postdata(); //クエリのリセット 
					?>

					<?php
					$wp_query = new WP_Query();
					$args = array(
						'posts_per_page' => -1, //表示する記事の数
						'post_type' => 'recruit', //投稿タイプ名
						'tax_query'      => array(
							array(
								'taxonomy' => 'recruit_status',
								'field' => 'slug',
								'terms' => 'part-time',
								'operator' => 'NOT IN',
							),
						)
					);
					$wp_query->query($args);
					?>
					<?php if ($wp_query->have_posts()) : ?>
						<?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
							<?php
							if ($homes = get_the_terms($post->ID, 'home')) {
								foreach ($homes as $home) {
									$home_slug = $home->slug;
									$home_name = $home->name;
								}
							}
							?>

							<div class="entry_area" id="<?php echo get_the_ID(); ?>">
								<div class="entry_area_inner">
									<h2 class="entry_mttl"> <?php the_title(); ?>の募集要項</h2>
									<table class="table_basic">
										<tr>
											<th>雇用内容</th>
											<td><?php the_title(); ?></td>
										</tr>
										<tr>
											<th>勤務時間</th>
											<td><?php the_field('recruit_time'); ?></td>
										</tr>
										<tr>
											<th>給与</th>
											<td><?php the_field('recruit_salary'); ?></td>
										</tr>
										<tr>
											<th>必要資格</th>
											<td><?php the_field('recruit_license'); ?></td>
										</tr>
										<tr>
											<th>待遇・福利厚生<br class="sp">など </th>
											<td><?php the_field('recruit_benefits'); ?></td>
										</tr>
										<tr>
											<th>休日休暇</th>
											<td><?php the_field('recruit_off'); ?></td>
										</tr>
									</table>
								</div>
							</div>
						<?php endwhile; ?>
					<?php else : //記事が無い場合 
					?>
					<?php endif; ?>

					<?php wp_reset_postdata(); //クエリのリセット 
					?>
					<h3 class="mttl"><span>エントリーする</span></h3>
					<p class="entry_text">採用についてのお問い合わせ・エントリーは以下のフォームをご利用ください。</p>
					<p class="entry_note"><span class="p_red">※</span>必須項目には必ずご記入ください。カタカナ・ひらがな・漢字は全角で、英数字は半角でご入力ください。</p>
					<p class="entry_text">「有」にチェックをされた方は、下の「お問い合わせ詳細」に詳しい内容をご記入ください。</p>
					<form action="mail.php" id="form" method="post" name="frmEntry" class="entry_form">
						<table class="table_basic entry_table">

							<?php
							$tax_name = 'home';
							$posttype_name = 'recruit';
							$terms = get_terms($tax_name);
							foreach ($terms as $term) :
								$index_slug = $term->slug;
								$index_name = $term->name;

								$args = array(
									'posts_per_page' => -1,
									'post_type' => $posttype_name,
									'tax_query' => array(
										array(
											'taxonomy' => $tax_name,
											'field'    => 'slug',
											'terms'    => $term->slug,
										),
										array(
											'taxonomy' => 'recruit_status',
											'field' => 'slug',
											'terms' => 'part-time',
											'operator' => 'NOT IN',
										),
									),
								);
								$the_query = new WP_Query($args);
							?>
								<?php if ($the_query->have_posts()) : ?>
									<tr>
										<th><?php echo $index_name ?></th>
										<td>
											<ul>
												<?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
													<?php
													$status_list = '';
													$jobs = get_the_terms($post->ID, 'recruit_status');
													$tmp = $jobs;
													if ($jobs) :
														foreach ($jobs as $job) :
															$status_list .= $job->name;
															if (next($tmp)) :
																$status_list .= '・';
															endif;
														endforeach;
													endif;
													?>
													<li>
														<label>
															<input name="<?php echo $index_name ?>[][、]" type="checkbox" value="<?php the_title(); ?>（<?php echo $status_list; ?>）" />
															<?php the_title(); ?>（<?php echo $status_list; ?>）</label>
													</li>
												<?php endwhile; ?>
											</ul>
										</td>
									</tr>
								<?php endif; ?>
							<?php endforeach;
							wp_reset_postdata(); ?>
							<tr>
								<th>ご希望職種の<br class="sp">経験の有無</th>
								<td><label>
										<input type="radio" name="ご希望職種の経験の有無[][、]" value="無" />
										無</label>
									<label>
										<input type="radio" name="ご希望職種の経験の有無[][、]" value="有" />
										有</label></td>
							</tr>
							<tr>
								<th> お名前<span class="required">※</span></th>
								<td><input name="お名前" class="inputarea_name validate[required]" type="text" />
									<span>（全角）</span>
								</td>
							</tr>
							<tr>
								<th> フリガナ <span class="required">※</span></th>
								<td><input name="フリガナ" class="inputarea_name validate[required]" type="text" />
									<span>（全角カタカナ）</span>
								</td>
							</tr>
							<tr>
								<th> 電話番号 <span class="required">※</span></th>
								<td><input name="電話番号" class="inputarea_name validate[required]" type="tel" />
									<span>（半角）</span>
								</td>
							</tr>
							<tr>
								<th> メールアドレス<span class="required">※</span></th>
								<td><input name="Email" class="inputarea_add validate[required]" type="email" placeholder="例）name@domain.co.jp" />
									（半角） </td>
							</tr>
							<tr>
								<th> ご住所</th>
								<td> 〒
									<input name="ご住所[][]" type="num" class="zip" size="10" maxlength="8" />
									<select name="ご住所[][]" size="1">
										<option selected="selected">都道府県</option>
										<option>北海道</option>
										<option>青森県</option>
										<option>岩手県</option>
										<option>宮城県</option>
										<option>秋田県</option>
										<option>山形県</option>
										<option>福島県</option>
										<option>茨城県</option>
										<option>栃木県</option>
										<option>群馬県</option>
										<option>埼玉県</option>
										<option>千葉県</option>
										<option>東京都</option>
										<option>神奈川県</option>
										<option>新潟県</option>
										<option>富山県</option>
										<option>石川県</option>
										<option>福井県</option>
										<option>山梨県</option>
										<option>長野県</option>
										<option>岐阜県</option>
										<option>静岡県</option>
										<option>愛知県</option>
										<option>三重県</option>
										<option>滋賀県</option>
										<option>京都府</option>
										<option>大阪府</option>
										<option>兵庫県</option>
										<option>奈良県</option>
										<option>和歌山県</option>
										<option>鳥取県</option>
										<option>島根県</option>
										<option>岡山県</option>
										<option>広島県</option>
										<option>山口県</option>
										<option>徳島県</option>
										<option>香川県</option>
										<option>愛媛県</option>
										<option>高知県</option>
										<option>福岡県</option>
										<option>佐賀県</option>
										<option>長崎県</option>
										<option>熊本県</option>
										<option>大分県</option>
										<option>宮崎県</option>
										<option>鹿児島県</option>
										<option>沖縄県</option>
									</select>
									<input name="ご住所[][]" type="text" class="address" size="55" maxlength="60" />
								</td>
							</tr>
							<tr>
								<th>ご年齢</th>
								<td><input size="10" type="tel" name="ご年齢" class="zip" />
									歳</td>
							</tr>
							<tr class="table_basic">
							  <th> ご希望の連絡方法</th>
							  <td><label>
							    <input name="連絡方法" type="radio" value="お電話" />
							    お電話</label>
							    <label>
							      <input name="連絡方法" type="radio" value="E-mail" />
							      E-mail</label>
							    <label>
							      <input checked="checked" name="連絡方法" type="radio" value="どちらでもよい" />
							      どちらでもよい</label></td>
						  </tr>
							<tr>
								<th>その他<br class="sp">ご相談など<br /></th>
								<td><textarea name="その他ご相談など" cols="50" rows="5"></textarea></td>
							</tr>
						</table>
						<p class="contact_mail_text">スペース・アズでは細心の注意を払って個人情報を管理・保護しております。お客様からいただくご連絡の内容につきましては、ご回答を差し上げるため、又は弊社からお客様へのより良いサービスのご提供などへの参考とさせていただく目的以外には利用いたしません。詳しくは「<a href="../info/privacy.html">個人情報保護方針</a>」のページをご参照ください。</p>
						<p class="contact_mail_btn">
							<input name="submit" type="submit" value="入力内容を確認" />
						</p>
					</form>
					<div class="entry_btn_back"><a href="../recruit/">採用情報へ戻る</a></div>
				</div>
			</div>
		</section>
		<!-- InstanceEndEditable --> </main>
<footer class="footer">
	<div class="inner">
		<div class="footer_top">
			<ul class="footer_info">
				<li class="tel">
					<h6 class="ttl">見学・入居のご相談・お問い合わせはお気軽に</h6>
					<a href="tel:0120834177"><img src="../parts/images/common/header_tel.png" alt="フリーダイヤル 0120-834-177"></a>
					<div class="time">お電話受付時間／9：00～17：00（土日祝休）</div>
				</li>
				<li class="contact"><a href="../info/">見学・体験入居・お問い合わせ</a></li>
			</ul>
			<nav class="footer_nav">
				<ul class="footer_nav_menu">
					<li><a href="../guidance/">施設のご紹介</a></li>
					<li><a href="../medical/">医療・介護体制</a></li>
					<li><a href="../flow/">ご入居までの流れ</a></li>
					<li><a href="../faq/">よくあるご質問</a></li>
					<li><a href="../about/">私たちについて</a></li>
					<li><a href="../recruit/">採用情報</a></li>
					<li><a href="../blog/">施設からのお知らせ</a></li>
					<li><a href="../info/privacy.html">個人情報保護方針</a></li>
				</ul>
				<ul class="footer_nav_group">
					<li class="real"><a href="../real/">不動産事業</a></li>
					<li class="ss"><a href="../ss/">老人ホーム事業</a></li>
				</ul>
			</nav>
		</div>
		<div class="footer_bottom">
			<h1 class="footer_logo"> <a href="../"><img src="../parts/images/common/footer_logo.png" alt=""> 奈良県・大阪府の有料老人ホームサービス付き高齢者向け住宅</a></h1>
			<div class="footer_address_wrap">
				<address class="footer_address">
				〒530-0046<br>大阪府大阪市北区菅原町11-10<br>オーキッド中之島402
				</address>
				<p class="footer_copy">Copyright (C)スペース・アズ. All Rights Reserved.</p>
			</div>
		</div>
	</div>
</footer>
    <!--191219_sphereリマケタグ-->	
<script src="https://rs.adapf.com/sr/7-I3Qfnke1eHDbCR9_7nmOl5EuRcmYonHw3s9BT00Po.js"></script>	
</body>
<!-- InstanceBeginEditable name="js2" -->
<script>
	function GethashID(hashIDName) {
		if (hashIDName) {
			$('.entry_tab li').find('a').each(function() {
				var idName = $(this).attr('href'); //
				if (idName == hashIDName) {
					var parentElm = $(this).parent();
					$('.entry_tab li').removeClass("active");
					$(parentElm).addClass("active");
					$(".entry_area").removeClass("is-active");
					$(hashIDName).addClass("is-active");
				}
			});
		}
	}
	$('.entry_tab a').on('click', function() {
		var idName = $(this).attr('href');
		GethashID(idName);
		return false;
	});
</script>
<!-- InstanceEndEditable -->	
<!-- InstanceEnd --></html>