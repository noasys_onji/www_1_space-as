<?php require('../blog/wp-load.php'); ?>
<!DOCTYPE html>
<html lang="ja"><!-- InstanceBegin template="/Templates/base.dwt" codeOutsideHTMLIsLocked="false" -->

<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
	<meta charset="UTF-8">
	<!-- InstanceBeginEditable name="title" -->
	<title>採用情報 ｜ スペース・アズ／奈良県生駒市・大阪府堺市の有料老人ホーム・デイサービス</title>
	<!-- InstanceEndEditable -->
	<meta name="viewport" content="width=device-width">
	<meta name="format-detection" content="telephone=no">
	<meta name="keywords" content="有料老人ホーム,ホーム,サ高住,訪問介護,大阪府,奈良県,プラスワンリビング,コージーガーデン,スペース・アズ。" />
	<meta name="description" content="有料老人ホーム・サ高住・訪問介護のスペース・アズ【奈良県・大阪府】。" />
	<!-- InstanceBeginEditable name="ogp" -->
	<meta property="og:title" content="採用情報 ｜ スペース・アズ／奈良県生駒市・大阪府堺市の有料老人ホーム・デイサービス" />
	<meta property="og:type" content="article" />
	<meta property="og:description" content="有料老人ホーム・サ高住・訪問介護のスペース・アズ【奈良県・大阪府】。" />
	<meta property="og:url" content="http://s-az.jp/recruit/" />
	<meta property="og:image" content="" />
	<meta property="og:locale" content="ja_JP" />
	<meta property="og:site_name" content="スペース・アズ／奈良県生駒市・大阪府堺市の有料老人ホーム・デイサービス" />
	<!-- InstanceEndEditable -->

	<!-- Google Fonts -->
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@300;400;500;700&family=Noto+Serif+JP&display=swap" rel="stylesheet">
	<!-- END Google Fonts -->
	<link rel="stylesheet" href="../parts/css/reset.css">
	<link rel="stylesheet" href="../parts/css/common.css">
	<!-- InstanceBeginEditable name="css" -->
	<link rel="stylesheet" href="../parts/css/recruit.css">
	<!-- InstanceEndEditable -->
	<script src="../parts/js/jquery-3.2.1.min.js"></script>
	<script src="../parts/js/script.js"></script>
	<!-- InstanceBeginEditable name="js" -->
	<!-- InstanceEndEditable -->
	<!-- InstanceParam name="en" type="text" value="recruit" -->




	<!-- Google tag (gtag.js) -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-3PTQBYDKR6"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'G-3PTQBYDKR6');
	</script>




</head>

<body class="recruit">
	<header class="header">
		<div class="inner_wide">
			<h1 class="header_logo"><a href="../"><img src="../parts/images/common/header_logo.png" alt="ロゴマーク"></a></h1>
			<ul class="header_info pc">
				<li class="tel"><a href="tel:0120834177"><img src="../parts/images/common/header_tel.png" alt="フリーダイヤル 0120-834-177"></a></li>
				<li class="contact"><a href="../info/">見学・体験入居・お問い合わせ</a></li>
			</ul>
			<nav class="gnav">
				<button type="button" class="gnav_button"></button>
				<div class="gnav_menu_wrapper">
					<ul class="gnav_menu">
						<li class="sp"> <a href="../">TOP</a> </li>
						<li> <a href="../guidance/">施設のご紹介</a> </li>
						<li> <a href="../medical/">医療・介護体制</a> </li>
						<li> <a href="../flow/">ご入居までの流れ</a> </li>
						<li> <a href="../faq/">よくあるご質問</a> </li>
						<li> <a href="../about/">私たちについて</a> </li>
						<li> <a href="../recruit/">採用情報</a> </li>
					</ul>
					<ul class="header_info sp">
						<li class="tel">
							<h6 class="ttl">見学・入居のご相談・お問い合わせはお気軽に</h6>
							<a href="tel:0120834177"><img src="../parts/images/common/header_tel.png" alt="フリーダイヤル 0120-834-177"></a>
							<div class="time">お電話受付時間／9：00～17：00（土日祝休）</div>
						</li>
						<li class="contact"><a href="../info/">見学・体験入居・お問い合わせ</a></li>
					</ul>
				</div>
			</nav>
		</div>
	</header>
	<main class="main"><!-- InstanceBeginEditable name="main" -->
		<section class="mv">
			<div class="inner">
				<h1 class="mv_ttl">採用情報</h1>
			</div>
		</section>
		<section class="intro">
			<div class="content">
				<div class="inner">
					<h2 class="intro_ttl">働きやすく進化する<br>介護の現場</h2>
					<p class="p_catch">信頼できる仲間・先輩の支え、<br>充実した育成制度とサポート。</p>
				</div>
			</div>
		</section>
		<section class="greeting">
			<div class="content gray wave">
				<div class="inner">
					<p class="p_catch">成長著しいマーケットで<br class="sp">ビジネス感覚を磨き、<br>みんなで新しい社会をつくっていこう。</p>
					<figure class="greeting_img"><img src="../parts/images/recruit/greeting_img.jpg" alt="スペース・アズ代表 村田 裕"></figure>
					<p class="greeting_text">2025年には、国民の10人に3人は65歳以上の高齢者になる―。</p>
					<p class="greeting_text">こうした数字から見えてくる未来はまさに、介護事業を手がける当社にとってのチャンスがひろがっています。介護業界での経験があれば、用語や法律はすぐに理解できるかもしれませんが、私たちが求めているのは業界での経験や専門知識ではなく、「この社会で、自分はいったい何ができるのか、会社の成長に自分がどう貢献できるのか」を本気で考えられる、信頼できるチームの一員です。</p>
					<p class="greeting_text">すぐに結果は求められません。どのような経験をお持ちの方でも、ゼロからチャレンジする気持ちでお越しください。当社のモットーは、現状に満足せずに「常に最善の方法を探す」ことです。新たな最善策を見つけたときに、それを受け入れて取り入れる素早さと、それに対応できる順応性を身につけてください。熱意を持っていただければ、個々のワークスキルを見極め、接遇・運営・営業・人事総務等、知識をピンポイントにお教えしますので、少しずつ仕事に慣れ、当社の社員としてのビジネス感覚を磨いてください。</p>
					<p class="greeting_text">会社の成長に携わる充実感も、キャリアアップの喜びも、時には挫折も。チームの一員として「共通の価値観」を共有していただける方をお待ちしています。</p>
					<p class="greeting_sign">スペース・アズ代表<span>村田 裕</span></p>
				</div>
			</div>
		</section>
		<section class="flow">
			<div class="content">
				<div class="inner">
					<h2 class="lttl_line"><span>スタッフの１日 </span></h2>
					<p class="p_catch">日勤スタッフの１日をご紹介</p>
					<ol class="flow_list">
						<li>
							<div class="time">9:30</div>
							<p class="task">各種訪問介護サービス</p>
							<p class="detail">入浴や排泄介助のサービス提供をしながら健康チェック。 </p>
						</li>
						<li>
							<div class="time">11:00</div>
							<p class="task">昼食の準備・声かけのため訪室</p>
						</li>
						<li>
							<div class="time">11:30</div>
							<p class="task">入居者様と一緒に食前体操</p>
						</li>
						<li>
							<div class="time">12:00</div>
							<p class="task">昼食</p>
							<p class="detail">昼食の配膳から必要な方へ食事介助、服薬介助。食後は居室へお送りします。 </p>
						</li>
						<li>
							<div class="time">13:00</div>
							<p class="task">各種訪問介護サービス</p>
							<p class="detail">お買い物同行や代行など、必要物品のチェック。</p>
						</li>
						<li>
							<div class="time">15:00</div>
							<p class="task">レクリエーション</p>
							<p class="detail">カラオケや季節のレクリエーションを行います。</p>
						</li>
						<li>
							<div class="time">16:00</div>
							<p class="task">夕食の準備・声かけのため訪室</p>
						</li>
						<li>
							<div class="time">17:00</div>
							<p class="task">夕食</p>
						</li>
						<li>
							<div class="time">18:00</div>
							<p class="task">退勤</p>
							<p class="detail">遅番や夜勤のスタッフに申し送りをし、1日の業務が終了します。</p>
						</li>
					</ol>
				</div>
			</div>
		</section>
		<section class="facility">
			<div class="content gray wave">
				<div class="inner">
					<h2 class="lttl_line"><span>現在募集中の施設</span></h2>
					<div class="facility_item" id="facility_ikoma">
						<h3 class="mttl"><span class="">生駒市</span>コージーガーデン生駒</h3>
						<div class="facility_wrap">
							<figure class="facility_img"><img src="../parts/images/recruit/img_ikoma.jpg" alt=""></figure>
							<div class="facility_textarea">
								<ul class="facility_btn">
									<li class="sei"><a href="entry_full.php">正社員・契約社員はこちら</a></li>
									<li class="pa"><a href="entry_part.php">パートタイムはこちら</a></li>
								</ul>
								<p class="facility_text">奈良県生駒市東菜畑2-973-13<br>近鉄生駒線「一分」駅より徒歩5分<br>本社：06-4709-5000（担当：総務）</p>

								<?php
								$wp_query = new WP_Query();
								$args = array(
									'posts_per_page' => -1, //表示する記事の数
									'post_type' => 'recruit', //投稿タイプ名
									'tax_query'      => array(
										array(
											'taxonomy' => 'home',
											'field' => 'slug',
											'terms' => 'ikoma',
										),
									)
								);
								$wp_query->query($args);
								?>
								<?php if ($wp_query->have_posts()) : ?>
									<ul class="facility_shokushu">
										<?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
											<?php
											$status_list = '';
											$jobs = get_the_terms($post->ID, 'recruit_status');
											$tmp = $jobs;
											if ($jobs) :
												foreach ($jobs as $job) :
													$status_name = $job->name;
													$status_slug = $job->slug;
													$status_list .= mb_substr($status_name, 0, 1);
													if (next($tmp)) :
														$status_list .= '・';
													endif;
												endforeach;
											endif;
											if ($status_slug == 'part-time') {
												$linkurl =  'entry_part.php';
											} else {
												$linkurl = 'entry_full.php';
											}
											?>
											<li><a href="<?php echo $linkurl; ?>?id=<?php echo get_the_ID(); ?>"><?php the_title(); ?>（<?php echo $status_list ?>）</a></li>
										<?php endwhile; ?>
									</ul>
								<?php else : //記事が無い場合 
								?>
									<p>ただいまの募集はございません。</p>
								<?php endif; ?>
							</div>
						</div>
						<!--<h4 class="facility_sttl">勤務風景</h4>-->
					</div>
					<div class="facility_item" id="facility_haze">
						<h3 class="mttl"><span class="">堺市</span>コージーガーデン土師（はぜ）</h3>
						<div class="facility_wrap">
							<figure class="facility_img"><img src="../parts/images/recruit/img_haze.jpg" alt=""></figure>
							<div class="facility_textarea">
								<ul class="facility_btn">
									<li class="sei"><a href="entry_full.php">正社員・契約社員はこちら</a></li>
									<li class="pa"><a href="entry_part.php">パートタイムはこちら</a></li>
								</ul>
								<p class="facility_text">大阪府堺市中区土師町3丁19-33<br>泉北高速鉄道線「深井」駅より徒歩13分<br>本社：06-4709-5000（担当：総務）</p>

								<?php
								$wp_query = new WP_Query();
								$args = array(
									'posts_per_page' => -1, //表示する記事の数
									'post_type' => 'recruit', //投稿タイプ名
									'tax_query'      => array(
										array(
											'taxonomy' => 'home',
											'field' => 'slug',
											'terms' => 'haze',
										),
									)
								);
								$wp_query->query($args);
								?>
								<?php if ($wp_query->have_posts()) : ?>
									<ul class="facility_shokushu">
										<?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
											<?php
											$status_list = '';
											$jobs = get_the_terms($post->ID, 'recruit_status');
											$tmp = $jobs;
											if ($jobs) :
												foreach ($jobs as $job) :
													$status_name = $job->name;
													$status_slug = $job->slug;
													$status_list .= mb_substr($status_name, 0, 1);
													if (next($tmp)) :
														$status_list .= '・';
													endif;
												endforeach;
											endif;
											if ($status_slug == 'part-time') {
												$linkurl =  'entry_part.php';
											} else {
												$linkurl = 'entry_full.php';
											}
											?>
											<li><a href="<?php echo $linkurl; ?>?id=<?php echo get_the_ID(); ?>"><?php the_title(); ?>（<?php echo $status_list ?>）</a></li>
										<?php endwhile; ?>
									</ul>
								<?php else : //記事が無い場合 
								?>
									<p>ただいまの募集はございません。</p>
								<?php endif; ?>
							</div>
						</div>
						<!--<h4 class="facility_sttl">勤務風景</h4>-->
					</div>
					<div class="facility_item" id="facility_neyagawa">
						<h3 class="mttl"><span class="">寝屋川市</span>コージーガーデン<br class="sp">東寝屋川</h3>
						<div class="facility_wrap">
							<figure class="facility_img"><img src="../parts/images/recruit/img_neyagawa.jpg" alt=""></figure>
							<div class="facility_textarea">
								<ul class="facility_btn">
									<li class="sei"><a href="entry_full.php">正社員・契約社員はこちら</a></li>
									<li class="pa"><a href="entry_part.php">パートタイムはこちら</a></li>
								</ul>
								<p class="facility_text">大阪府寝屋川市打上中町14番48号<br>JR学研都市線「寝屋川公園」駅より徒歩7分<br>本社：06-4709-5000（担当：総務）</p>

								<?php
								$wp_query = new WP_Query();
								$args = array(
									'posts_per_page' => -1, //表示する記事の数
									'post_type' => 'recruit', //投稿タイプ名
									'tax_query'      => array(
										array(
											'taxonomy' => 'home',
											'field' => 'slug',
											'terms' => 'e_neyagawa',
										),
									)
								);
								$wp_query->query($args);
								?>
								<?php if ($wp_query->have_posts()) : ?>
									<ul class="facility_shokushu">
										<?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
											<?php
											$status_list = '';
											$jobs = get_the_terms($post->ID, 'recruit_status');
											$tmp = $jobs;
											if ($jobs) :
												foreach ($jobs as $job) :
													$status_name = $job->name;
													$status_slug = $job->slug;
													$status_list .= mb_substr($status_name, 0, 1);
													if (next($tmp)) :
														$status_list .= '・';
													endif;
												endforeach;
											endif;
											if ($status_slug == 'part-time') {
												$linkurl =  'entry_part.php';
											} else {
												$linkurl = 'entry_full.php';
											}
											?>
											<li><a href="<?php echo $linkurl; ?>?id=<?php echo get_the_ID(); ?>"><?php the_title(); ?>（<?php echo $status_list ?>）</a></li>
										<?php endwhile; ?>
									</ul>
								<?php else : //記事が無い場合 
								?>
									<p>ただいまの募集はございません。</p>
								<?php endif; ?>
							</div>
						</div>
						<!--<h4 class="facility_sttl">勤務風景</h4> -->
					</div>
					<div class="facility_item" id="facility_neyagawa_2">
						<h3 class="mttl"><span class="">寝屋川市</span>コージーガーデン<br class="sp">東寝屋川Ⅱ番館</h3>
						<div class="facility_wrap">
							<figure class="facility_img"><img src="../parts/images/recruit/img_neyagawa_2.jpg" alt=""></figure>
							<div class="facility_textarea">
								<ul class="facility_btn">
									<li class="sei"><a href="entry_full.php">正社員・契約社員はこちら</a></li>
									<li class="pa"><a href="entry_part.php">パートタイムはこちら</a></li>
								</ul>
								<p class="facility_text">寝屋川市打上中町3番3号<br>JR東西線「寝屋川公園」駅 徒歩11分<br>本社：06-4709-5000（担当：総務）</p>

								<?php
								$wp_query = new WP_Query();
								$args = array(
									'posts_per_page' => -1, //表示する記事の数
									'post_type' => 'recruit', //投稿タイプ名
									'tax_query'      => array(
										array(
											'taxonomy' => 'home',
											'field' => 'slug',
											'terms' => 'neyagawa_park',
										),
									)
								);
								$wp_query->query($args);
								?>
								<?php if ($wp_query->have_posts()) : ?>
									<ul class="facility_shokushu">
										<?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
											<?php
											$status_list = '';
											$jobs = get_the_terms($post->ID, 'recruit_status');
											$tmp = $jobs;
											if ($jobs) :
												foreach ($jobs as $job) :
													$status_name = $job->name;
													$status_slug = $job->slug;
													$status_list .= mb_substr($status_name, 0, 1);
													if (next($tmp)) :
														$status_list .= '・';
													endif;
												endforeach;
											endif;
											if ($status_slug == 'part-time') {
												$linkurl =  'entry_part.php';
											} else {
												$linkurl = 'entry_full.php';
											}
											?>
											<li><a href="<?php echo $linkurl; ?>?id=<?php echo get_the_ID(); ?>"><?php the_title(); ?>（<?php echo $status_list ?>）</a></li>
										<?php endwhile; ?>
									</ul>
								<?php else : //記事が無い場合 
								?>
									<p>ただいまの募集はございません。</p>
								<?php endif; ?>
							</div>
						</div>
						<!--<h4 class="facility_sttl">勤務風景</h4> -->
					</div>
				</div>
			</div>
		</section>
		<section class="message">
			<div class="content gray wave">
				<div class="inner">
					<h2 class="lttl_line"><span>スタッフからのメッセージ</span></h2>
					<ul class="message_list">
						<li>
							<figure class="message_img"><img src="../parts/images/recruit/message_01.png" alt=""></figure>
							<div class="message_name"><span class="name">入社6年目</span><span class="position">エリア統括</span></div>
							<p class="message_text">施設内の安全と快適な環境づくりに努めるとともに、経験豊富な管理者として、ご入居者さまの皆様に寄り添ったサービスを提供しています。様々な活動やイベントを企画・運営していますので、楽しい時間を共有し、入居者さまの心の豊かさをサポートいたします。</p>
						</li>
						<li>
							<figure class="message_img"><img src="../parts/images/recruit/message_02.png" alt=""></figure>
							<div class="message_name"><span class="name">入社6年目</span><span class="position">訪問介護員</span></div>
							<p class="message_text">日常生活でのお手伝いや介護を行っています。個々のニーズや希望に寄り添い、尊厳と安心を守りながらお手伝いをさせていただきます。温かな笑顔と専門知識を活かし、入居者さまの安心と快適な生活をサポートするために全力を尽くします。</p>
						</li>
					</ul>
				</div>
			</div>
		</section>
		<section class="entry">
			<div class="content">
				<div class="inner">
					<h2 class="lttl_line"><span>エントリーフォーム</span></h2>
					<ul class="entry_btn_list">
						<li class="entry_btn sei"><a href="entry_full.php">正社員・契約社員</a></li>
						<li class="entry_btn pa"><a href="entry_part.php">パートタイム</a></li>
					</ul>
				</div>
			</div>
		</section>
		<nav class="float">
			<h2 class="float_ttl"><img src="../parts/images/recruit/float_ttl.png" alt="カンタン応募！！"></h2>
			<ul>
				<!--<li><a href="#recruiting"><img src="../parts/images/recruit/float_recruiting.png" alt="募集職種"></a></li>-->
				<li><a href="entry_part.php"><img src="../parts/images/recruit/float_pa.png" alt="パートタイム"></a></li>
				<li><a href="entry_full.php"><img src="../parts/images/recruit/float_sei.png" alt="正社員・契約社員"></a></li>
			</ul>
		</nav>
		<!-- InstanceEndEditable -->
	</main>
	<footer class="footer">
		<div class="inner">
			<div class="footer_top">
				<ul class="footer_info">
					<li class="tel">
						<h6 class="ttl">見学・入居のご相談・お問い合わせはお気軽に</h6>
						<a href="tel:0120834177"><img src="../parts/images/common/header_tel.png" alt="フリーダイヤル 0120-834-177"></a>
						<div class="time">お電話受付時間／9：00～17：00（土日祝休）</div>
					</li>
					<li class="contact"><a href="../info/">見学・体験入居・お問い合わせ</a></li>
				</ul>
				<nav class="footer_nav">
					<ul class="footer_nav_menu">
						<li><a href="../guidance/">施設のご紹介</a></li>
						<li><a href="../medical/">医療・介護体制</a></li>
						<li><a href="../flow/">ご入居までの流れ</a></li>
						<li><a href="../faq/">よくあるご質問</a></li>
						<li><a href="../about/">私たちについて</a></li>
						<li><a href="../recruit/">採用情報</a></li>
						<li><a href="../blog/">施設からのお知らせ</a></li>
						<li><a href="../info/privacy.html">個人情報保護方針</a></li>
					</ul>
					<ul class="footer_nav_group">
						<li class="real"><a href="../real/">不動産事業</a></li>
						<li class="ss"><a href="../ss/">老人ホーム事業</a></li>
					</ul>
				</nav>
			</div>
			<div class="footer_bottom">
				<h1 class="footer_logo"> <a href="../"><img src="../parts/images/common/footer_logo.png" alt=""> 奈良県・大阪府の有料老人ホームサービス付き高齢者向け住宅</a></h1>
				<div class="footer_address_wrap">
					<address class="footer_address">
						〒530-0046<br>大阪府大阪市北区菅原町11-10<br>オーキッド中之島402
					</address>
					<p class="footer_copy">Copyright (C)スペース・アズ. All Rights Reserved.</p>
				</div>
			</div>
		</div>
	</footer>
	<!--191219_sphereリマケタグ-->
	<script src="https://rs.adapf.com/sr/7-I3Qfnke1eHDbCR9_7nmOl5EuRcmYonHw3s9BT00Po.js"></script>
</body>
<!-- InstanceBeginEditable name="js2" -->
<!-- InstanceEndEditable -->
<!-- InstanceEnd -->

</html>