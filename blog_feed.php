<?php require('blog/wp-load.php'); ?>

<?php
$wp_query = new WP_Query();
$args = array(
	'posts_per_page' => 8, //表示する記事の数
	'post_type' => 'post', //投稿タイプ名
);
$wp_query->query($args);
?>
<?php if ($wp_query->have_posts()) : ?>
	<?php while ($wp_query->have_posts()) : $wp_query->the_post();
		$cat = get_the_category();
		$cat_name = $cat[0]->cat_name; ?>
		<li class="blog_list_item"><a href="<?php the_permalink(); ?>">
				<figure><?php if (has_post_thumbnail()) : ?>
						<?php the_post_thumbnail('thumbnail'); ?>
					<?php else : ?>
						<img src="<?php echo catch_that_image(); ?>" alt="" />
					<?php endif; ?>
				</figure>
				<div class="blog_list_textarea">
					<time class="blog_list_date"><?php the_time('Y/m/d'); ?> </time>
					<h3 class="blog_list_ttl"><?php the_title(); ?></h3>
					<span class="blog_list_category"><?php echo $cat_name; ?></span>
				</div>
			</a></li>
	<?php endwhile; ?>
<?php else : //記事が無い場合 
?>
<?php endif; ?>

<?php wp_reset_postdata(); //クエリのリセット 
?>