</div>





<div id="footer_wrapper">
	<ul id="footnavi">
		<li><a href="/index.html">ホーム</a>
		</li>
		<li><a href="/guidance/index.html">施設のご紹介</a>
		</li>
		<li><a href="/feature/index.html">施設の特長</a>
		</li>
		<li><a href="/medical/index.html">医療・介護体制</a>
		</li>
		<li><a href="/flow/index.html">ご入居までの流れ</a>
		</li>
		<li><a href="/about/index.html">私たちについて</a>
		</li>
		<li><a href="/recruit/index.html">採用情報</a>
		</li>
		<li><a href="/blog/">施設からのお知らせ</a>
		</li>
		<li><a href="/info/contact.html">各種お問い合わせ</a>
		</li>
		<li><a href="/info/privacy.html">個人情報保護方針</a>
		</li>
		<li><a href="/info/sitemap.html">サイトマップ</a>
		</li>
	</ul>
</div>


<div id="footer_wrapper2">
	<h2>運営施設のご案内</h2>

	<div id="foot_guide">

		<div>
			<p><a href="/guidance/home_ikoma/index.html"><img src="/common/images/foot_cozy_ikoma.jpg" alt="施設写真" /></a>
			</p>
			<p>住宅型有料老人ホーム</p>
			<h3><a href="/guidance/home_ikoma/index.html">コージーガーデン生駒</a></h3>
		</div>

		<div>
			<p><a href="/guidance/home_haze/index.html"><img src="/common/images/foot_cozy_haji.jpg" alt="施設写真" /></a>
			</p>
			<p>住宅型有料老人ホーム</p>
			<h3><a href="/guidance/home_haze/index.html">コージーガーデン土師</a></h3>
		</div>

		<div>
			<p><a href="/guidance/home_e_neyagawa/index.html"><img src="/common/images/foot_home_e_neyagawa.jpg" alt="施設写真" /></a>
			</p>
			<p>サービス付高齢者向け住宅</p>
			<h3><a href="/guidance/home_e_neyagawa/index.html">コージーガーデン東寝屋川</a></h3>
		</div>

		<div class="real">
			<p><a href="/real/"><img src="/common/images/foot_real_img.jpg" alt="" /></a>
			</p>
			<h3><a href="/real/"><img src="/common/images/foot_real_text.png" alt=""></a></h3>
		</div>
		<div class="ss">
			<p><a href="/ss/"><img src="/common/images/foot_ss_img.jpg" alt="" /></a>
			</p>
			<h3><a href="/ss/"><img src="/common/images/foot_ss_text.png" alt=""></a></h3>
		</div>
		<!--<div class="cp">
		<p><a href="/cp/"><img src="/common/images/foot_cp_img.jpg" alt="" /></a></p>
		<h3><a href="/cp/"><img src="/common/images/foot_cp_text.png" alt=""></a></h3>
	</div>-->
	</div>


</div>


<div id="footer_wrapper3">
	<div id="footer">
		<p id="foot_logo"><a href="/index.html"><img src="/common/images/foot_logo.gif" alt="スペース アズ" /></a>
		</p>
		<p id="foot_txt">奈良県・大阪府の有料老人ホーム<br/> サービス付き高齢者向け住宅
		</p>
		<p id="foot_tel"><img src="/common/images/foot_tel.gif" alt="0120-834-177"/>
		</p>
		<p id="foot_info"><strong>見学・入居のご相談・お問い合わせはお気軽に</strong><br/> お電話受付時間／9：00～17：00（土日祝休）
		</p>
	</div>
</div>


<p id="copyright"><img src="/common/images/foot_copy.gif" alt=""/>
</p>
<!--191219_sphereリマケタグ-->	
<script src="https://rs.adapf.com/sr/7-I3Qfnke1eHDbCR9_7nmOl5EuRcmYonHw3s9BT00Po.js"></script>

</body>
</html>