<?php get_header(); ?>
<?php
	if ( have_posts() ) :
	while (have_posts() ) : the_post();
?>
		<div class="container">
			<div class="col-sm-8">
				<section class="site-content" role="main">
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?></articl>
						<header>
							<h2 class="entry-title"><a href="<?php the_permalink(); ?>" ><?php the_title(); ?></a></h2>
							<section class="entry-meta"> <span class="author vcard"><i class="fa fa-user"></i><?php the_author_posts_link(); ?></span> <span class="meta-sep"> </span> <span class="entry-date"><i class="fa fa-calendar"></i><?php the_time('Y年n月j日'); ?></span> </section>
						</header>
						<section class="entry-content">
							<?php the_content(); ?>
						</section>
						<footer class="entry-footer"> <span class="cat-links"><i class="fa fa-folder"></i> <?php the_category(); ?></span> <span class="tag-links"></span> <span class="edit-link"></span> </footer>
					</article>
					<nav id="nav-below" class="navigation" role="navigation">
						<div class="nav-previous"><span class="meta-nav">&larr;</span><?php previous_post_link( '%link' ); ?> </div>
						<div class="nav-next"></span><?php next_post_link( '%link' ); ?><span class="meta-nav">&rarr; </div>
					</nav>
				</section>
			</div>
		</div>

<?php
	endwhile;
	endif;
?>
<?php get_sidebar(); ?>
<?php get_footer(); ?>