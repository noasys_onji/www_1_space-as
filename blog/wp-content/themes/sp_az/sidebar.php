</div>


<div id="sidebar">
	<div class="col-sm-4">
		<aside id="sidebar" role="complementary">
			<div id="primary" class="widget-area">
				<ul>
					<li>
						<h3 class="widget-title">最近の投稿</h3>
						<ul class="site-sidebar">
							<?php
								$args = array(
								'posts_per_page' => 5 // 表示件数の指定
								);
								$posts = get_posts( $args );
								foreach ( $posts as $post ): // ループの開始
								setup_postdata( $post ); // 記事データの取得
							?>
							<li>
								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</li>
								<?php
								endforeach; // ループの終了
								wp_reset_postdata(); // 直前のクエリを復元する
							?>
						</ul>
					</li>
					<li>
						<h3 class="widget-title">アーカイブ</h3>
						<ul class="site-sidebar">
							<?php
								$args = array(
								'type' => 'monthly',
								'show_post_count' => true,
								);
								wp_get_archives( $args );
							?>
						</ul>
					</li>
				</ul>
			</div>
		</aside>
	</div>	
	<p id="side_map"><img src="/common/images/areamap_defo.png" alt="エリアマップ" border="0" usemap="#Map" id="Image1"/>
		<map name="Map" id="Map">
			<area shape="poly" coords="70,142,110,117,78,111,79,96,115,96,113,84,129,81,117,51,112,38,125,38,145,46,162,74,149,138" href="/guidance/index.html#area_osaka" onmouseover="MM_swapImage('Image1','','/common/images/areamap_osaka.png',0)" onmouseout="MM_swapImgRestore()" />
			<area shape="poly" coords="142,207,187,203,202,191,222,124,188,80,173,87,161,85,148,140,159,153,130,180" href="/guidance/index.html#area_nara" onmouseover="MM_swapImage('Image1','','/common/images/areamap_nara.png',0)" onmouseout="MM_swapImgRestore()" />
		</map>
	</p>
	<p id="side_contact"><a href="/info/contact.html"><img src="/common/images/side_contact.jpg" alt="各種お問い合わせ窓口" /></a>
	</p>
	<ul class="bnr">
		<li><a href="/blog/"><img src="/common/images/bnr_news.jpg" alt="お知らせ" /></a>
		</li>
		<li><a href="/about/greeting.html"><img src="/common/images/bnr_idea.jpg" alt="私たちの理念" /></a>
		</li>
		<li><a href="/about/message.html"><img src="/common/images/bnr_staffvoice.jpg" alt="スタッフの声" /></a>
		</li>
		<li><a href="/real/"><img src="/common/images/side_nav_real.png" alt="不動産事業" /></a>
		</li>
		<li><a href="/ss/"><img src="/common/images/side_nav_ss.png" alt="老人ホーム紹介事業" /></a>
		</li>
		<!--<li class="close"><a href="/cp/"><img src="/common/images/side_nav_cp.png" alt="ケアプラン事業" /></a></li>-->
	</ul>
</div>