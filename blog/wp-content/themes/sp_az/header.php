<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<!-- 191219_Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-97331415-6"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-97331415-6');
	</script>		
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>
		<?php wp_title( ' | ', true, 'right' ); ?>｜スペース・アズ／奈良県生駒市・大阪府堺市の有料老人ホーム・デイサービス</title>
	<meta name="Description" content="有料老人ホーム・デイサービスのスペース・アズ【奈良県生駒市・大阪府堺市】"/>
	<meta name="Keywords" content="有料老人ホーム,ホーム・デイサービス,大阪市,奈良県生駒市,プラスワンリビング,コージーガーデン,スペース・アズ"/>
	<link href="/common/css/common.css" rel="stylesheet" type="text/css"/>
	<script src="/common/js/jquery-1.7.2.min.js"></script>
	<script src="/common/js/idalink.js"></script>
	<script src="/common/js/heightLine.js"></script>
	<script src="/common/js/areamap.js"></script>
</head>

<body id="top">
	<div class="header_top">
		<h1 class="summary">有料老人ホーム・デイサービスのスペース・アズ【奈良県生駒市・大阪府堺市】</h1>
		<ul id="header_nav">
			<li><a href="../real/"><img src="../common/images/head_nav_real.png" alt="不動産事業"></a>
			</li>
			<li><a href="../ss/"><img src="../common/images/head_nav_ss.png" alt="老人ホーム紹介事業"></a>
			</li>
			<!--<li class="close"><a href="../cp/"><img src="../common/images/head_nav_cp.png" alt="ケアプラン事業"></a></li>-->
		</ul>
	</div>
	<div id="header">
		<p id="logo"><a href="/index.html"><img src="/common/images/logo.jpg" alt="スペース・アズ　まごころで、つつむ、介護。" /></a>
		</p>
		<ul id="header_sub">
			<li><a href="/info/contact.html"><img src="/common/images/head_contact.png" alt="お問い合わせ" /></a>
			</li>
			<li><img src="/common/images/head_tel.jpg" alt="見学・入居のご相談・お問い合わせはお気軽に0120-834-177"/>
			</li>
		</ul>
	</div>


	<ul id="gnavi">
		<li><a href="/index.html"><img src="/common/images/g_home.jpg" alt="ホーム" /></a>
		</li>
		<li><a href="/guidance/index.html"><img src="/common/images/g_info.jpg" alt="施設のご紹介" /></a>
		</li>
		<li><a href="/feature/index.html"><img src="/common/images/g_feature.jpg" alt="施設の特長" /></a>
		</li>
		<li><a href="/medical/index.html"><img src="/common/images/g_medical.jpg" alt="医療・介護体制" /></a>
		</li>
		<li><a href="/flow/index.html"><img src="/common/images/g_frow.jpg" alt="ご入居までの流れ" /></a>
		</li>
		<li><a href="/about/index.html"><img src="/common/images/g_about.jpg" alt="私たちについて" /></a>
		</li>
		<li><a href="/recruit/index.html"><img src="/common/images/g_recruit.jpg" alt="採用情報" /></a>
		</li>
	</ul>

	<div id="midmainblog">
		<h1><img src="/common/images/h1blog.png" alt="お知らせ" /></h1>
	</div>



	<div id="container">

		<div id="mid_content">
			<ul id="pan">
				<li><a href="/index.html">ホーム</a>
				</li>
				<li>お知らせ</li>
			</ul>