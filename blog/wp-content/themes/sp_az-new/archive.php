<?php get_header(); ?>


<section class="mv">
	<div class="inner">
		<h1 class="mv_ttl">施設からのお知らせ</h1>
	</div>
</section>
<section class="blog" id="blogarea">
	<div class="content index_content">
		<div class="inner">
			<h2 class="lttl_line"><span>施設別表示</span></h2>
			<ul class="blog_category">
				<li><a href="/blog/">全ての記事を表示</a></li>
				<?php
				$curCat = $catid[0]->cat_ID;
				$args = array(
					'title_li' => ''
				);
				wp_list_categories($args);
				?>
			</ul>
			<?php if (have_posts()) : ?>
				<ul class="blog_list">
					<?php while (have_posts()) : the_post(); ?>
						<?php
						$cat = get_the_category();
						$cat = $cat[0];
						$cat_name = $cat->cat_name;
						$cat_slug = $cat->slug;
						?>
						<li class="blog_list_item"><a href="<?php the_permalink(); ?>">
								<figure><?php if (has_post_thumbnail()) : ?>
										<?php the_post_thumbnail('thumbnail'); ?>
									<?php else : ?>
										<img src="<?php echo catch_that_image(); ?>" alt="" />
									<?php endif; ?>
								</figure>
								<div class="blog_list_textarea">
									<time class="blog_list_date"><?php the_time('Y/m/d'); ?> </time>
									<h3 class="blog_list_ttl"><?php the_title(); ?></h3>
									<span class="blog_list_category"><?php echo $cat_name; ?></span>
								</div>
							</a></li>

					<?php endwhile; ?>
					<div class="nav-below">
						<?php if (function_exists('wp_pagenavi')) {
							wp_pagenavi();
						} ?>
					</div><!-- /.nav-below -->
				</ul>
			<?php endif; ?>
		</div>
	</div>
</section>
<?php get_footer(); ?>