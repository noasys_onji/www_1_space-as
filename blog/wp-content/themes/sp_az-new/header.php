<!DOCTYPE html>
<html lang="ja">

<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
	<meta charset="UTF-8">
	<title><?php wp_title(' | ', true, 'right'); ?>｜スペース・アズ／奈良県生駒市・大阪府堺市の有料老人ホーム・デイサービス</title>
	<meta name="viewport" content="width=device-width">
	<meta name="format-detection" content="telephone=no">
	<meta name="keywords" content="有料老人ホーム,ホーム,サ高住,訪問介護,大阪府,奈良県,プラスワンリビング,コージーガーデン,スペース・アズ。" />
	<meta name="description" content="有料老人ホーム・サ高住・訪問介護のスペース・アズ【奈良県・大阪府】。" />
	<meta property="og:title" content="施設のご紹介 ｜ スペース・アズ／奈良県生駒市・大阪府堺市の有料老人ホーム・デイサービス" />
	<meta property="og:type" content="article" />
	<meta property="og:description" content="有料老人ホーム・サ高住・訪問介護のスペース・アズ【奈良県・大阪府】。" />
	<meta property="og:url" content="http://s-az.jp/blog/" />
	<meta property="og:image" content="" />
	<meta property="og:locale" content="ja_JP" />
	<meta property="og:site_name" content="スペース・アズ／奈良県生駒市・大阪府堺市の有料老人ホーム・デイサービス" />

	<!-- Google Fonts -->
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@300;400;500;700&family=Noto+Serif+JP&display=swap" rel="stylesheet">
	<!-- END Google Fonts -->
	<link rel="stylesheet" href="/parts/css/reset.css">
	<link rel="stylesheet" href="/parts/css/common.css">
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
	<link rel="stylesheet" href="/parts/css/blog.css">
	<script src="/parts/js/jquery-3.2.1.min.js"></script>
	<script src="/parts/js/script.js"></script>
	<!-- Google tag (gtag.js) -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-3PTQBYDKR6"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'G-3PTQBYDKR6');
	</script>
	<?php wp_head(); ?>
</head>

<body class="guidance">
	<header class="header">
		<div class="inner_wide">
			<h1 class="header_logo"><a href="/"><img src="/parts/images/common/header_logo.png" alt="ロゴマーク"></a></h1>
			<ul class="header_info pc">
				<li class="tel"><a href="tel:0120834177"><img src="/parts/images/common/header_tel.png" alt="フリーダイヤル 0120-834-177"></a></li>
				<li class="contact"><a href="/info/">見学・体験入居・お問い合わせ</a></li>
			</ul>
			<nav class="gnav">
				<button type="button" class="gnav_button"></button>
				<div class="gnav_menu_wrapper">
					<ul class="gnav_menu">
						<li class="sp"> <a href="/">TOP</a> </li>
						<li> <a href="/guidance/">施設のご紹介</a> </li>
						<li> <a href="/medical/">医療・介護体制</a> </li>
						<li> <a href="/flow/">ご入居までの流れ</a> </li>
						<li> <a href="/faq/">よくあるご質問</a> </li>
						<li> <a href="/about/">私たちについて</a> </li>
						<li> <a href="/recruit/">採用情報</a> </li>
					</ul>
					<ul class="header_info sp">
						<li class="tel">
							<h6 class="ttl">見学・入居のご相談・お問い合わせはお気軽に</h6>
							<a href="tel:0120834177"><img src="/parts/images/common/header_tel.png" alt="フリーダイヤル 0120-834-177"></a>
							<div class="time">お電話受付時間／9：00～17：00（土日祝休）</div>
						</li>
						<li class="contact"><a href="/info/">見学・体験入居・お問い合わせ</a></li>
					</ul>
				</div>
			</nav>
		</div>
	</header>
	<main class="main">