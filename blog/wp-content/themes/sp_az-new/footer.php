</main>
<footer class="footer">
	<div class="inner">
		<div class="footer_top">
			<ul class="footer_info">
				<li class="tel">
					<h6 class="ttl">見学・入居のご相談・お問い合わせはお気軽に</h6>
					<a href="tel:0120834177"><img src="/parts/images/common/header_tel.png" alt="フリーダイヤル 0120-834-177"></a>
					<div class="time">お電話受付時間／9：00～17：00（土日祝休）</div>
				</li>
				<li class="contact"><a href="/info/">見学・体験入居・お問い合わせ</a></li>
			</ul>
			<nav class="footer_nav">
				<ul class="footer_nav_menu">
					<li><a href="/guidance/">施設のご紹介</a></li>
					<li><a href="/medical/">医療・介護体制</a></li>
					<li><a href="/flow/">ご入居までの流れ</a></li>
					<li><a href="/faq/">よくあるご質問</a></li>
					<li><a href="/about/">私たちについて</a></li>
					<li><a href="/recruit/">採用情報</a></li>
					<li><a href="/blog/">施設からのお知らせ</a></li>
					<li><a href="/info/privacy.html">個人情報保護方針</a></li>
				</ul>
				<ul class="footer_nav_group">
					<li class="real"><a href="/real/">不動産事業</a></li>
					<li class="ss"><a href="/ss/">老人ホーム事業</a></li>
				</ul>
			</nav>
		</div>
		<div class="footer_bottom">
			<h1 class="footer_logo"> <a href="/"><img src="/parts/images/common/footer_logo.png" alt=""> 奈良県・大阪府の有料老人ホームサービス付き高齢者向け住宅</a></h1>
			<div class="footer_address_wrap">
				<address class="footer_address">
					〒530-0046<br>大阪府大阪市北区菅原町11-10<br>オーキッド中之島402
				</address>
				<p class="footer_copy">Copyright (C)スペース・アズ. All Rights Reserved.</p>
			</div>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>
<!--191219_sphereリマケタグ-->
<script src="https://rs.adapf.com/sr/7-I3Qfnke1eHDbCR9_7nmOl5EuRcmYonHw3s9BT00Po.js"></script>
</body>

</html>