<?php

function setup_theme()
{
	add_theme_support('post-thumbnails');
}
add_action('after_setup_theme', 'setup_theme');

function catch_that_image()
{
	global $post, $posts;
	$first_img = '';
	ob_start();
	ob_end_clean();
	$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
	$first_img = $matches[1][0];

	if (empty($first_img)) {
		// 記事内で画像がなかった時のための画像を指定（ディレクトリ先や画像名称は任意）
		$first_img = "/assets/img/index/img_thumb.png";
	}

	return $first_img;
}
