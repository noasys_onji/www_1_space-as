<?php get_header(); ?>


<section class="mv">
	<div class="inner">
		<h1 class="mv_ttl">施設からのお知らせ</h1>
	</div>
</section>
<section class="">
	<div class="content">
		<div class="inner">
			<?php
			if (have_posts()) :
				while (have_posts()) : the_post();
			?>

					<article class="article">
						<h2 class=""><?php the_title(); ?></h2>
						<div class="article_info">
							<div class="category"><?php the_category(); ?></div>
							<div class=""><span class="editor"><?php the_author_posts_link(); ?></span>
								<time class="date"><?php the_time('Y年n月j日'); ?></time>
							</div>
						</div>

						<?php the_content(); ?>

					</article>

				<?php
				endwhile;
				?>

				<nav class="post_nav_list">
					<div class="prev"><?php if (get_previous_post()) : ?><?php previous_post_link('%link', '前の記事へ'); ?><?php endif; ?>&nbsp;</div>
					<div class="back"><a href="/blog/">一覧へ戻る</a></div>
					<div class="next"><?php if (get_next_post()) : ?><?php next_post_link('%link', '次の記事へ'); ?><?php endif; ?>&nbsp;</div>
				</nav>
			<?php
			endif;
			?>
		</div>
	</div>
</section>

<?php get_footer(); ?>