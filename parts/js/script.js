/*ドロワーメニュー*/

$(function () {
	
	$('.gnav_button').click(function () {
		$(this).toggleClass('active');
		$('.gnav_bg').fadeToggle();
		$('.gnav_menu_wrapper').toggleClass('open');
	})
	$('.gnav_bg').click(function () {
		$(this).fadeOut();
		$('.gnav_button').removeClass('active');
		$('.gnav_menu_wrapper').removeClass('open');
	});
})

/*スムーズスクロール*/

$(function(){
	$('a[href^="#"]').click(function(){
		var Hash = $(this.hash);
		var HashOffset = $(Hash).offset().top;
		$("html,body").animate({
			scrollTop: HashOffset
		}, 1000);
		return false;
	});
});