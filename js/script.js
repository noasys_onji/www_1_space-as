$(function(){

	$("#slider").bxSlider({
		displaySlideQty: 3,
		speed: 1000,
		pause: 6000,
		moveSlideQty: 1,
		auto: true,
		autoControls: false,
        pager: true,
		controls: true
	});

});